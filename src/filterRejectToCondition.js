import isEmpty from 'lodash/isEmpty';

export default function filterRejectToCondition([toFilter, toReject], requireBoth=true) {
  const condition = {};
  if (!isEmpty(toFilter)) {
    if(requireBoth) {

      condition.$all = toFilter;
    }
    else {

    condition.$in = toFilter;
    }
  }
  if (!isEmpty(toReject)) {
    condition.$nin = toReject;
  }
  if (isEmpty(condition)) {
    return;
  }
  // eslint-disable-next-line consistent-return
  return condition;
}
