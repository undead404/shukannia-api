import supertest from 'supertest';
import { disconnect } from './mongodb';
import server from './server';

// eslint-disable-next-line jest/no-hooks, jest/require-top-level-describe
afterAll(disconnect);
describe('api /', () => {
  it('responds', async () => {
    expect.assertions(1);
    try {
      const response = await supertest(server).get('/');
      expect(response.statusCode).toStrictEqual(200);
      // return Promise.resolve();
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  });
  it('gives not more than 10', async () => {
    expect.assertions(1);
    try {
      const response = await supertest(server).get('/');
      //   const data = JSON.parse(response.body);
      expect(response.body.length).toBeLessThanOrEqual(10);
      // return Promise.resolve();
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  });
});
describe('api /?kind=:kind&genre=:genre', () => {
  it('responds', async () => {
    expect.assertions(1);
    try {
      const response = await supertest(server).get('/?genre=short');
      expect(response.statusCode).toStrictEqual(200);
      // return Promise.resolve();
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  });
  it('gives not more than 10', async () => {
    expect.assertions(1);
    try {
      const response = await supertest(server).get('/?genre=short');
      //   const data = JSON.parse(response.body);
      expect(response.body.length).toBeLessThanOrEqual(10);
      // return Promise.resolve();
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  });
});
describe('api /?kind=:kind&genre=:genre&limit=:limit', () => {
  it('responds', async () => {
    expect.assertions(1);
    try {
      const response = await supertest(server).get('/?genre=action&limit=5');
      expect(response.statusCode).toStrictEqual(200);
      // return Promise.resolve();
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  });
  it('gives not more than 10', async () => {
    expect.assertions(1);
    try {
      const response = await supertest(server).get('/?genre=action&limit=5');
      //   const data = JSON.parse(response.body);
      expect(response.body.length).toBeLessThanOrEqual(5);
      // return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }
  });
});

describe('api /title/:id', () => {
  it('responds with data for present IDs', async () => {
    expect.assertions(3);
    try {
      const response = await supertest(server).get('/title/1');
      expect(response.statusCode).toStrictEqual(200);
      expect(response.body).toHaveProperty('imdbId');
      expect(response.body.imdbId).toStrictEqual(1);
      // return Promise.resolve();
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  });
  it('responds with 404 for non-present IDs', async () => {
    expect.assertions(1);
    try {
      const response = await supertest(server).get('/title/1243546586978709');
      expect(response.statusCode).toStrictEqual(404);
      // return Promise.resolve();      
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  });
});
