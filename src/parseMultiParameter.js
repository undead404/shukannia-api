import forEach from 'lodash/forEach';
import split from 'lodash/split';
import startsWith from 'lodash/startsWith';

export default function parseMultiParameter(multiParameter) {
  if (!multiParameter) {
    return;
  }
  const tokens = split(multiParameter, ',');
  const toFilter = [];
  const toReject = [];
  forEach(tokens, token => {
    if (startsWith(token, '!')) {
      toReject.push(token.slice(1));
    } else {
      toFilter.push(token);
    }
  });
  // eslint-disable-next-line consistent-return
  return [toFilter, toReject];
}
