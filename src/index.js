import server from './server';

export default server.listen(process.env.PORT || 3013);
