import path from 'path';
import Bugsnag from '@bugsnag/js';
import BugsnagPluginExpress from '@bugsnag/plugin-express';
import cors from 'cors';
import dotenv from 'dotenv';
import express from 'express';
import ExpressLRUCache from 'express-lru-cache';
import get from 'lodash/get';
import includes from 'lodash/includes';
import isEmpty from 'lodash/isEmpty';
import isNaN from 'lodash/isNaN';
import { getTitleByImdbId, getMovies } from './mongodb';
import parseMultiParameter from './parseMultiParameter';

const ALLOWED_ORIGINS = ['http://localhost:3000', 'https://shukannia.vercel.app'];

// eslint-disable-next-line import/no-dynamic-require,unicorn/prefer-module
const package_ = require(path.join(__dirname, '..', 'package.json'));
dotenv.config();
const NODE_ENV = process.env.NODE_ENV;


const app = express();
const cache = new ExpressLRUCache();
let middleware;
if (NODE_ENV !== 'test') {
  let appVersion = package_.version;
  if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    appVersion += '-dev';
  }
  Bugsnag.start({
    apiKey: process.env.BUGSNAG_API_KEY,
    appVersion,
    plugins: [BugsnagPluginExpress]
  });
  // bugsnagClient.use(bugsnagExpress);
  middleware = Bugsnag.getPlugin('express');
  app.use(middleware.requestHandler);
}
app.use(cache.middleware());

app.use(
  cors({
    origin(origin, callback) {
      if (!origin) {
        return callback(null, true);
      }
      if (includes(ALLOWED_ORIGINS, origin)) {
        return callback(null, true);
      }
      return callback(
        new Error(
          'The CORS policy for this site does not allow access from the specified Origin.',
        ),
        false,
      );
    },
  }),
);
app.get('/title/:imdbId', async (request, response) => {
  try {
    const titleImdbId = Number.parseInt(get(request, 'params.imdbId'), 10);
    if (isNaN(titleImdbId)) {
      return response.status(400).json({ error: 'imdbId should be numeric' });
    }
    const title = await getTitleByImdbId(titleImdbId);
    if (!title) {
      return response.status(404).json({ error: 'Title not found' });
    }
    return response.status(200).json(title);
  } catch (error) {
    return response
      .status(500)
      .json({ error: isEmpty(error) ? 'Unknown error' : error });
  }
});
app.get('/', async (request, response) => {
  const {
    country,
    genre,
    kind,
    language,
    limit: limitParameter = 10,
    order = 'top',
  } = request.query;
  try {
    const limit = Number.parseInt(limitParameter, 10);
    if (isNaN(limit)) {
      return response.status(400).json({ error: 'limit should be numeric' });
    }
    const best = await getMovies({
      country: parseMultiParameter(country),
      genre: parseMultiParameter(genre),
      kind: parseMultiParameter(kind),
      language: parseMultiParameter(language),
      limit,
      order,
    });
    return response.status(200).json(best);
  } catch (error) {
    return response.status(500).json({ error });
  }
});
if (NODE_ENV !== 'test') {
  // This handles any errors that Express catches
  app.use(middleware.errorHandler);
}

export default app;
