import dotenv from 'dotenv';
import get from 'lodash/get';
import map from 'lodash/map';
import { MongoClient } from 'mongodb';
import getWeightCalculator from './getWeightCalculator';
import filterRejectToCondition from './filterRejectToCondition';
import logger from './logger';

dotenv.config();
const uri = `mongodb+srv://${process.env.MONGODB_USER_NAME}:${process.env.MONGODB_USER_PASSWORD}@cluster0-2cmol.mongodb.net/shukannia?retryWrites=true&w=majority`;
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
let collection;
export default client;
async function init() {
  if (!collection) {
    logger.info('connect');
    const connection = await client.connect();
    collection = connection.db('shukannia').collection('titles');
  }
}
const titleCache = new Map();
export async function getTitleByImdbId(imdbId) {
  if(titleCache.has(imdbId))  {
    return titleCache.get(imdbId);
  }
  await init();
  const query = { imdbId };
  logger.info('findOne');
  logger.debug(JSON.stringify(query, null, 2));
  const result = collection.findOne(query);
  if( result) {
    titleCache.set(imdbId, result)
  }
  return result;
}

async function populateSeries(title) {
  if (title.seriesId) {
    const series = await getTitleByImdbId(title.seriesId);
    return {
      ...title,
      series,
    };
  }
  return title;
}
export async function getMovies({
  country,
  genre,
  kind = [null, ['tv_series', 'video_game']],
  language,
  limit,
  order = 'top',
}) {
  await init();
  const query = {
    kind: filterRejectToCondition(kind, false),
    releasedAt: {
      $lte: new Date(),
    },
  };
  if (country) {
    query.countries = filterRejectToCondition(country);
  }
  if (genre) {
    query.genres = filterRejectToCondition(genre);
  }
  if (language) {
    query.languages = filterRejectToCondition(language);
  }
  const aggregation = [
    {
      $match: query,
    },
    {
      $project: {
        powerRating: {
          $multiply: ['$rating', '$votesNum'],
        },
        votesNum: true,
      },
    },
    {
      $group: {
        _id: null,
        powerRatingSum: {
          $sum: '$powerRating',
        },
        votesNumSum: {
          $sum: '$votesNum',
        },
      },
    },
    {
      $project: {
        meanRating: {
          $divide: ['$powerRatingSum', '$votesNumSum'],
        },
      },
    },
  ];
  logger.info('aggregate')
  logger.debug(JSON.stringify(aggregation, null, 2))
  const result = await collection
    .aggregate(aggregation)
    .toArray();
  const meanRating = get(result, '[0].meanRating');
  const weightCalculator = getWeightCalculator(order, meanRating);
  logger.debug(JSON.stringify(weightCalculator, null, 2))
  const aggregation2 = [
    {
      $match: query,
    },
    {
      $project: {
        countries: true,
        description: true,
        episode: true,
        explicit: true,
        genres: true,
        image: true,
        imdbId: true,
        kind: true,
        languages: true,
        name: true,
        rating: true,
        releasedAt: true,
        season: true,
        seriesId: true,
        severe: true,
        updatedAt: true,
        votesNum: true,
        weight: weightCalculator,
      },
    },
    {
      $sort: {
        weight: -1,
      },
    },
    {
      $limit: limit,
    },
    {
      $match: {
        weight: {
          $gte: 0,
        },
      },
    },
  ];
  if(order !== 'recent') {
    aggregation2.push(
      {
        $sort: {
          releasedAt: 1,
          season: 1,
          episode: 1,
          name: 1,
        },
      },)
  }
  const titles = await collection
    .aggregate(aggregation2)
    .toArray();
  return Promise.all(map(titles, populateSeries));
}

export function disconnect() {
  logger.info('close')
  client.close();
}