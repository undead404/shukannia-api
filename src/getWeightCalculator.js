export default function getWeightCalculator(order = 'top', meanRating) {
  switch (order) {
    case 'bottom':
      return {
        $multiply: [{ $subtract: [meanRating, '$rating'] }, '$votesNum'],
      };
    case 'fresh-garbage':
      return {
        $divide: [
          {
            $multiply: [{ $subtract: [meanRating, '$rating'] }, '$votesNum'],
          },
          {
            $subtract: [new Date(), '$releasedAt'],
          },
        ],
      };
    case 'fresh-good':
      return {
        $divide: [
          {
            $multiply: [{ $subtract: ['$rating', meanRating] }, '$votesNum'],
          },
          {
            $subtract: [new Date(), '$releasedAt'],
          },
        ],
      };
    case 'old-garbage':
      return {
        $multiply: [
          {
            $multiply: [{ $subtract: [meanRating, '$rating'] }, '$votesNum'],
          },
          {
            $subtract: [new Date(), '$releasedAt'],
          },
        ],
      };
    case 'old-good':
      return {
        $multiply: [
          {
            $multiply: [{ $subtract: ['$rating', meanRating] }, '$votesNum'],
          },
          {
            $subtract: [new Date(), '$releasedAt'],
          },
        ],
      };
      case 'recent':
        return {
          $divide: [
            '$votesNum',
            {
              $subtract: [new Date(), '$releasedAt']
            }
          ]
        }
    case 'top':
      return {
        $multiply: [{ $subtract: ['$rating', meanRating] }, '$votesNum'],
      };
    case 'weird':
      return {
        $divide: [
          '$votesNum',
          { $abs: { $subtract: ['$rating', meanRating] } },
        ],
      };
    default:
      throw new Error('Unknown order');
  }
}
