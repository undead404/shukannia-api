module.exports = {
  env: {
    commonjs: true,
    node: true,
  },
  extends: [
    'airbnb-base',
    'plugin:promise/recommended',
    'plugin:unicorn/recommended',
    'plugin:array-func/all',
    'plugin:lodash/recommended',
    'plugin:node/recommended',
    'plugin:eslint-comments/recommended',
    'plugin:jest/all',
    'prettier',
  ],
  overrides: [
    {
      files: ['./*.js', 'scripts/*'],
      rules: {
        'import/no-extraneous-dependencies': [
          'error',
          {
            devDependencies: true,
          },
        ],
        'node/no-unpublished-import': 'off',
        'unicorn/prefer-module': 'off'
      },
    },
    {
      env: {
        'jest/globals': true,
      },
      files: ['*.test.js'],
      rules: {
        'lodash/prefer-constant': 'off',
        'react/jsx-props-no-spreading': 'off',
        'node/no-unpublished-import': 'off',
        'consistent-return': 'off'
      },
    },
    {
      files: ['mongodb.js'],
      rules: {
        'lodash/prefer-lodash-method': ['error', { ignoreMethods: ['find'] }],
      },
    },
  ],
  parser: '@babel/eslint-parser',
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
  },
  plugins: [
    'prettier',
    'promise',
    'unicorn',
    'array-func',
    'lodash',
    'node',
    'eslint-comments',
    'jest',
  ],
  rules: {
    'no-console': 0,
    'no-param-reassign': ['error', { props: false }],
    'prefer-destructuring': 0,
    'consistent-return': ['warn', { treatUndefinedAsUnspecified: true }],
    'arrow-body-style': 0,
    'comma-dangle': 0,
    'node/no-unsupported-features/es-syntax': 'off',
    'unicorn/filename-case': [
      'error',
      {
        cases: { camelCase: true, pascalCase: true },
      },
    ],
    'no-underscore-dangle': 'off',
    'jest/no-test-callback': 'off',
    'unicorn/no-null': 'off',
    'unicorn/prefer-node-protocol': 'off'
  },
};
