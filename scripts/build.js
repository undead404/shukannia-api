import childProcess from 'child_process';
import fs from 'fs';
import path from 'path';
import reportBuild from 'bugsnag-build-reporter';
import dotenv from 'dotenv';
import rimraf from 'rimraf';

dotenv.config();

const packageJson = fs.readFileSync(path.join(__dirname, '..', 'package.json'));
const package_ = JSON.parse(packageJson);

rimraf.sync(path.join(__dirname, '..', 'build'));
childProcess.execSync(
  `babel src --out-dir=${path.join(__dirname, '..', 'build')} --copy-files`,
);
childProcess.execSync(
  `cp package.json ${path.join(__dirname, '..', 'build')}/`
)
childProcess.execSync(
  `cp yarn.lock ${path.join(__dirname, '..', 'build')}/`
)
childProcess.execSync(
  `cp Procfile ${path.join(__dirname, '..', 'build')}/`
)
reportBuild({
  apiKey: process.env.BUGSNAG_API_KEY,
  appVersion: package_.version,
  releaseStage: process.env.NODE_ENV || 'development',
});
